import axios from '@/plugins/axios';
// import Api from '@/components/Api.js'
export default {
    async signIn ({ dispatch }, credentials) {
        let respuesta
        await axios.post('/api/login', credentials).then((response) => {
            respuesta = response
            if(response.data.success){
                dispatch('headers', response.data)
                return dispatch('me')
            }
        })
        return respuesta
    },

    async signOut ({ dispatch }) {
        await axios.post('/api/logout', {}, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(()=>{
            return dispatch('logout')
        })
    },

    me ({ commit}) {
        let respuesta = null;
        let token = localStorage.getItem('token')
        if (token && token !== "") {
            commit('SET_TOKEN', token)
            axios.defaults.headers.common = {'Authorization': `Bearer ${token}`}
            return axios.get('/api/user?_relations[0]=proyecto.archivo_logo&_relations[1]=proyecto.archivo_icono').then((response) => {
                respuesta = response
                if(response.data.item.id){
                    commit('SET_AUTHENTICATED', true)
                    commit('SET_USER', response.data.item)
                    commit('SET_PERMISOS', response.data.item.permissions)
                }
                return respuesta
            }).catch(() => {

                commit('SET_AUTHENTICATED', false)
                commit('SET_USER', [])
                commit('SET_TOKEN', '')
            })
        }else{
            commit('SET_AUTHENTICATED', false)
            commit('SET_USER', [])
            commit('SET_TOKEN', '')
        }

        return respuesta
    },

    headers ({ commit}, payload) {
        commit('SET_TOKEN', payload.access_token)
        axios.defaults.headers.common = {'Authorization': `Bearer ${payload.access_token}`}
    },

    logout({commit}){
        localStorage.setItem("token", "");
        location.href="/"
        setTimeout(()=>{
            commit('SET_AUTHENTICATED', false)
            commit('SET_USER', [])
            commit('SET_TOKEN', "")
        },300)
    },
    getProyectos({ commit }){
        axios.get('api/admin/proyectos').then((response)=>{
            commit('SET_PROYECTOS', response.data.items);
        })
    },
    clearProyecto({commit}){
        commit('SET_PROYECTO', []);
    },
}
