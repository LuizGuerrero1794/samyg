import Vue from 'vue'
import Vuex from 'vuex'

import index from './store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        index: index,
    }
})

export default vx;
