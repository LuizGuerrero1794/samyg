import Vue from 'vue'

export default {
    SET_AUTHENTICATED(state, value) {
        state.authenticated = value
    },

    SET_USER(state, value) {
        state.user = value
    },

    SET_TOKEN(state, value) {
        localStorage.setItem('token', value);
        state.token = value
    },
    setDataTables(state, data) {
        Vue.set(state.datatables, data.key, data.value)
    },
    SET_PROYECTOS(state, data) {
        let proyectos = [];

        data.map(p => {
            proyectos.push(p);
        })

        state.catalogo_proyectos = proyectos;
    },
    SET_PROYECTO(state, data) {
        state.proyecto = data;
    },
    SET_BREADCRUMB(state, data) {
        if (state.breadcrumbs.length > 1) {
            state.breadcrumbs.pop()
        }

        state.breadcrumbs.push(data);
    },

    SET_PERMISOS(state, data) {
        let permisos = [];
        data.map(permiso => {
            permisos.push(permiso.name)
        })
        Vue.prototype.$gates.setPermissions(permisos)
        state.permisos = permisos

    }
}
