export default {
    datatables: {},
    authenticated: false,
    user: [],
    catalogo_proyectos: [],
    proyecto: [],
    token: '',
    rules:{
        required: v => !!v || "CAMPO REQUERIDO",
        moreCero: v => v>0 || "NO PUEDE CONTENER UN VALOR MENOR O IGUAL A CERO",
        moreCien: v => v<=100 || "NO PUEDE CONTENER UN VALOR MAYOR AL 100 %",
        movil: v => !!v || "FORMATO INCORRECTO",
        min_8: v => (v || '').length >= 8 || "MINIMO 8 (OCHO) CARACTERES",
        min_5: v => (v || '').length >= 5 || "MINIMO 5 (CINCO) CARACTERES",
        rfc: value => {
            const pattern = /^^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$/;
            return pattern.test(value) || 'EL FORMATO DEL R.F.C. ES INVALIDO';
        },
        curp: value=> {
            const pattern = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/
            return pattern.test(value) || 'EL FORMATO DE LA C.U.R.P. ES INVALIDO'
        },
        numbers: value => {
            const pattern = /[0-9]$/
            return pattern.test(value) ||'SOLO SE ADMITEN NUMEROS'
        },
        decimal: value => {
            const pattern = /^([0-9]+\.?[0-9]{0,2})$/
            return pattern.test(value) || 'SOLO SE ADMITEN NUMEROS DECIMALES'
        },
        email: value => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(value) || 'EL CORREO ELECTRONICO ES INVALIDO',
        icon: value=> value.type=="image/x-icon" || 'EL ARCHIVO SELECCIONADO, DEBE SER UN ARCHIVO ".icon"'
    },
    breadcrumbs: [{
        text: 'Proyectos',
        icon: 'mdi-home',
        disabled: false,
        href: '/',
    }],
    permisos: [],
}
