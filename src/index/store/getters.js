export default {
    authenticated (state) {
        return state.authenticated
    },
    user (state) {
        return state.user
    },
    token (state) {
        return state.token
    },
    rules (state){
        return state.rules;
    },
    sistema (state){
        return state.sistema;
    },
    breadcrumbs (state){
        return state.breadcrumbs;
    },
    proyecto(state){
        return state.proyecto;
    },
    catalogo_proyectos (state){

        return state.catalogo_proyectos;
    },
    baseURL (state){
        return state.baseURL;
    },
    permisos (state){
        return state.permisos
    },
}
