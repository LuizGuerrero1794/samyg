import Vue from 'vue'
import Vuex from 'vuex'

import nms from './store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        nms: nms,
    }
})

export default vx;
