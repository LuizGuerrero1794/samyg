// import store from '@/plugins/store.js'
// import axios from '@/plugins/axios.js'

export default [
    {
        path: '/',
        name: 'Administracion',
        redirect: '/administracion/inicio',
        component: () => import('@/administracion/Full.vue'),
        children: [
            {
                path: '/administracion/inicio',
                name: 'Administracion-Inicio',
                component: ()=>import('@/administracion/views/Dashboard.vue'),
            },
            {
                path: '/administracion/usuarios',
                name: 'Administracion-Usuarios',
                component: () => import('@/administracion/views/Usuarios/Index.vue'),
            },
            {
                path: '/administracion/sitios',
                name: 'Administracion-Sitios',
                component: () => import('@/administracion/views/Sitios/Index.vue'),
            },
            {
                path: '/administracion/configuracion',
                name: 'Administracion-Configuracion',
                component: () => import('@/administracion/views/Configuracion/Index.vue'),
            },
        ]
    }
]
