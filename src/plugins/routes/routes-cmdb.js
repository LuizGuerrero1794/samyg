
export default [
        {
            path: '/',
            name: 'CMDB',
            redirect: '/infraestructura',
            component: () => import('@/CMDB/Full.vue'),
            children: [
                {
                    path: 'infraestructura',
                    name: 'Infraestructura',
                    component: () => import('@/CMDB/views/Infraestructura/Index.vue'),
                },
                {
                    path: 'dispositivos',
                    name: 'Dispositivos',
                    component: ()=>import('@/CMDB/views/Dispositivos/Index.vue'),
                },
                {
                    path: '/configuracion',
                    name: 'Configuracion',
                    component: () => import('@/CMDB/views/Configuracion/Index.vue'),
                },
            ]
        }
    ]
