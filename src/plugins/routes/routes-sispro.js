
export default [
        {
            path: '/',
            name: 'SISPRO',
            redirect: '/fallas',
            component: () => import('@/sispro/Full.vue'),
            children: [
                // {
                //     path: 'dashboard',
                //     name: 'Dashboard',
                //     component: () => import('@/sispro/views/Dashboard.vue'),
                // },
                {
                    path: 'fallas',
                    name: 'Fallas',
                    component: () => import('@/sispro/views/Fallas.vue'),
                },
                {
                    path: 'perfomance',
                    name: 'Perfomance',
                    component: () => import('@/sispro/views/Perfomance.vue'),
                },
                {
                    path: 'sitios',
                    name: 'Sitios',
                    component: () => import('@/sispro/views/Sitios.vue'),
                },
                {
                    path: 'reportes',
                    name: 'Reportes',
                    component: () => import('@/sispro/views/Reportes.vue'),
                },
            ]
        }
    ]
