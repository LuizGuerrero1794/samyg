
export default [
        {
            path: '/',
            name: 'NMS',
            redirect: 'nms/inicio',
            component: () => import('@/nms/Full.vue'),
            children: [
                {
                    path: 'nms/inicio',
                    name: 'Inicio',
                    component: () => import('@/nms/views/Index.vue'),
                    children:[
                        {
                            path: '/dashboard',
                            name: 'Dashboard',
                            component: () => import('@/nms/views/PanelOperaciones/Dashboard.vue'),
                        },
                        {
                            path: '/mapa',
                            name: 'Mapa de Red',
                            component: () => import('@/nms/views/PanelOperaciones/MapaDeRed.vue'),
                        }
                    ],
                },
                {
                    path: 'nms/discovery',
                    name: 'Discovery',
                    component: () => import('@/nms/views/Discovery'),
                },
            ]
        }
    ]
