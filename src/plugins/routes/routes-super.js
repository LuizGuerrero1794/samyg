// import store from '@/plugins/store.js'
// import axios from '@/plugins/axios.js'

export default [
    {
        path: '/',
        name: 'index',
        redirect: '/proyectos',
        component: () => import('@/index/Full.vue'),
        children:[
            {
                path: '/proyectos',
                name: 'Proyectos',        
                component: () => import('@/index/views/Index.vue'),
            }
        ]
    },
    {
        path: '/proyecto/:id',
        name: 'Administracion',
        redirect: { name: "Administracion-Administradores" },
        component: () => import('@/administracion/Full.vue'),
        children:[
            {
                path: '/proyecto/:id/administradores',
                name: 'Administracion-Administradores',        
                component: () => import('@/administracion/views/Administradores/Index.vue'),
            },
            {
                path: '/proyecto/:id/configuracion',
                name: 'Administracion-Configuracion',        
                component: () => import('@/administracion/views/Configuracion/Index.vue'),
            }
        ]
    }
    
]