import axios from 'axios'
//import {vm} from '@/main.js'
let token = localStorage.getItem('token')

const instance = axios.create({
    baseURL: process.env.VUE_APP_API_HOST, // Local
    headers: {
        common : {
            'Authorization': `Bearer ${token}`,
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type' : 'application/json',
            'Accept' : 'application/json'
        }
    }
});

// Add a response interceptor
instance.interceptors.response.use(
    response => {
        //this.loading = false;
        return response;
    },
    error => {

        if (error.response.status === 401) {

            //vm.$router.push({name: 'auth.login'});

        } else if (error.response.status === 422 || error.response.status === 429) {

            if (error.response.data.errors){
                /*
                for(let key in error.response.data.errors){

                    //app.$validator.errors.add({field: key, msg: error.response.data.errors[key]})
                }

                 */
            }

        } else {

            console.error(error)
        }
        //this.loading = false;

        return Promise.reject(error);
    })

export default instance
