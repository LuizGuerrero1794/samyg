export const MONEY = {
    data: function(){
        return {
            money: {
              decimal: '.',
              thousands: ',',
              precision: 2,
              masked: true
            },
        }
    },
    methods: {
    }
}

