import {mapActions} from 'vuex'

export const FUNCTIONS = {
    data: function () {
        return {}
    },
    methods: {
        ...mapActions({
            fetchFiles: 'conquest/fetchFiles',
        }),

        openFile(file) {
            this.$notify({
                text: 'Procesando...',
                type: 'info'
            });
            console.log(file);

            this.axios.request({
                url: 'api/conquest/files/get_file',
                data: file,
                method: 'POST',
                responseType: 'blob',
            }).then((response) => {
                console.log(response);
                //let file = new Blob([response.data], {type: 'application/vnd.ms-excel'});
                let new_file = new Blob([response.data], {type: file.tipo});
                //blob.name = "imageFilename.png"
                const url = window.URL.createObjectURL(new_file);
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', file.nombre);
                link.setAttribute('id', 'file-download');
                //link.setAttribute('name', 'file-download');
                link.setAttribute('target', '_blank');
                document.body.appendChild(link);

                link.click();
            });
        },

        deleteFile(file){
            this.$notify({
                text: 'Procesando...',
                type: 'info'
            });

            let data = {id: file.id}
            this.axios.post('files.delete_file', data)
                .then(({data}) => {
                    this.$notify({
                        text: data.message,
                        type: 'info'
                    });
                })
                .catch(response => {
                    this.$notify({
                        text: response.message,
                        type: 'error'
                    });
                    console.log(response);
                }).finally(() => {
                    this.fetchFiles({ticket: file.origenable_id})
            })
        },
    }
}
