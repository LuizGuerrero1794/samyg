export const NOTIFY = {
    data: function(){
        return {
            /* Notification Settings */
            show: false,
            message: '',
            color: '',
        }
    },
    methods: {
        notify(message, color) {
            this.message = message
            this.color = color
            this.show = true
        },
    }
}

