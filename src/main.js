require("./plugins/bootstrap");

import Vue from 'vue'
import store from "./plugins/store";
import vuetify from "./plugins/vuetify";
import Notifications from 'vue-notification'

//routes
import super_admin from '@/plugins/routes/routes-super.js';
import admin from '@/plugins/routes/routes-admin.js';
import cmdb from '@/plugins/routes/routes-cmdb.js';
import auth from '@/plugins/routes/routes-auth.js';
import conquestAdmin from '@/conquest/routes/router-admin.js';
import conquest from '@/conquest/routes/router.js';
import notFound from '@/plugins/routes/routes-not-found.js';
import nms from '@/plugins/routes/routes-nms.js';
import sispro from '@/plugins/routes/routes-sispro.js';

import VueGates from 'vue-gates';
Vue.use(VueGates, {
    persistent: true,
});

import VueFusionCharts from 'vue-fusioncharts';

// import FusionCharts modules and resolve dependency
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import RealTimeLine from "fusioncharts/fusioncharts.widgets";

Vue.use(VueFusionCharts, FusionCharts, Charts, FusionTheme, RealTimeLine);

import axios from '@/plugins/axios'
import Sidebar from '@/components/Sidebar.js'
import VueAxios from 'vue-axios'
import App from '@/App.vue';
import VueRouter from 'vue-router';

// let baseURL = 'http://samyg.sytes.net' // Produccion
//  let baseURL = process.env.VUE_APP_API_HOST // Local



Vue.use(VueAxios, axios)

import VueSocketIO from 'vue-socket.io'

Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://192.168.1.5:3000',
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    },
    options: { query: "keyMunEnt=1" } //Optional options
}))

import './plugins/filters'

Vue.use(Notifications)

window.EventBus = new Vue();

Vue.config.productionTip = false
Vue.use(VueRouter);

// store.state.index.baseURL = baseURL;

store.dispatch('index/me').then(() => {
    
    let route = [];


    if(store.state.index.authenticated){
        
        let role = store.state.index.user.roles[0];

        store.state.index.proyecto = store.state.index.user.proyecto;

        switch(role.name){
            case 'super_administrador':{
                route = [...super_admin, ...notFound];
                store.state.administracion.sidebar = Sidebar.get(role.name);
                break;
            }
            case 'adm_administrador':{
                route = [...admin, ...notFound];
                store.state.administracion.sidebar = Sidebar.get(role.name);

                break;
            }
            case 'nms_administrador':{
                route = [...nms, ...notFound];
                // store.state.nms.sidebar = Sidebar.get(role.name);
                // store.state.index.proyecto = store.state.index.user.proyecto;
                break;
            }
            case 'cmdb_administrador':{
                route = [...cmdb,...notFound];
                store.state.cmdb.sidebar = Sidebar.get(role.name);

                break;
            }
            case 'sispro_administrador':{
                route = [...sispro,...notFound];
                break;
            }
            case 'cmdb_usuario':{
                route = [...cmdb,...notFound];
                store.state.cmdb.sidebar = Sidebar.get(role.name);
                break;
            }
            case 'cq_administrador':
                route = [...conquestAdmin, ...notFound];
                break;
            case 'cq_usuario':
            case 'cq_operador':
            case 'cq_soporte':
            case 'cq_supervisor':
                route = [...conquest, ...notFound];
                break;
        }
        
    }else{
        route = [...auth];
    }

    const router = new VueRouter({
        mode: 'history',
        linkActiveClass: '',
        base: document.querySelector('#app').getAttribute('data-path') || '/',
        scrollBehavior: () => ({y: 0}),
        store,
        routes: route,    
    })

    new Vue({
    store,
    router,
    vuetify,
    render: h => h(App),
    }).$mount('#app')
})