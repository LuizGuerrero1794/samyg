import Vue from 'vue'
import Vuex from 'vuex'

//import Auth from './auth/store'
//import Administracion from './administracion/store'
//import Dashboard from './dashboard/store'
//import Tickets from './tickets/store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        //auth: Auth,
        //administracion: Administracion,
        //dashboard: Dashboard,
        //tickets: Tickets ,
    }
})

export default vx;
