export default {
    fetchUser({commit}, payload) {
        commit('SET_USER', payload)
    },
    fetchRoles({commit}, payload) {
        commit('SET_ROLES', payload)
    },
    fetchPermissions({commit}, payload) {
        commit('SET_PERMISSIONS', payload)
    },
}
