export default {
    getUserName: state => {
        return state.user.name
    },
    getUserEmail: state => {
        return state.user.email
    },
    getRoles: state => {
        return state.roles
    },
    getPermissions: state => {
        return state.permissions
    },
}
