export default {
    SET_USER(state, payload) {
        state.user = payload
    },
    SET_ROLES(state, payload) {
        state.roles = payload
    },
    SET_PERMISSIONS(state, payload) {
        state.permissions = payload
    },
}
