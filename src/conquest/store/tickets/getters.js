export default {
    getFiles: state => {
        return state.files
    },

    getTicket: state => {
        return state.ticket
    },

    getSeguimiento: state => {
        return state.seguimiento
    },
}
