import axios from '@/plugins/axios.js'
let route = 'api/conquest/'

export default {

    async fetchFiles({commit}, {ticket}) {
        let res
        if(ticket){
            res = await axios.get(route+'files/find_by_ticket/'+ticket)
        }else{
            res = await axios.get(route+'files/index')
        }
        commit('SET_FILES', res.data.files.items)
    },

    async fetchTicket({commit}, {ticket}) {
        let res = await axios.get(route+'tickets/'+ ticket)
        commit('SET_TICKET', res.data.ticket)
    },

    updateTicket({commit}, payload){
        commit('SET_TICKET', payload);
    },

    async fetchSeguimiento({commit}, {ticket}) {
        let res = await axios.get(route+'seguimiento/list/'+ticket)
        commit('SET_SEGUIMIENTO', res.data.items)
    },

}
