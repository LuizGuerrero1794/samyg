export default {
    SET_FILES(state, payload) {
        state.files = payload
    },

    SET_TICKET(state, payload){
        state.ticket = payload
    },

    SET_SEGUIMIENTO(state, payload){
        state.seguimiento = payload
    }
}
