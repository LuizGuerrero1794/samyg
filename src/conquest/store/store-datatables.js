import Vue from "vue";
import Vuex from "vuex";

export default new Vuex.Store({
    state: {
        datatables: {}
    },
    mutations: {
        setDataTables(state, data) {
            Vue.set(state.datatables, data.key, data.value)
        },
    },
});
