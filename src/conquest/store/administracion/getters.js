export default {
    getSeveridades: state => {
        return state.severidades
    },

    getMedios: state => {
        return state.medios
    },

    getEstatus: state => {
        return state.estatus
    },

    getTiposReporte: state => {
        return state.tipos_reporte
    },

    getUsuarios: state => {
        return state.usuarios
    },

    getOperadores: state => {
        return state.operadores
    },

    getSoportes: state => {
        return state.soportes
    },

    getDispositivos: state => {
        return state.dispositivos
    },

    getSitios: state => {
        return state.sitios
    },

    getPartidas: state => {
        return state.partidas
    },
}
