export default {
    sistema: {
        bg_color: 'blue darken-4',
    },
    severidades: [],
    medios: [],
    tipos_reporte: [],
    estatus: [],
    usuarios: [],
    operadores: [],
    soportes: [],
    dispositivos: [],
    sitios: [],
    partidas: [],
}
