import axios from '@/plugins/axios.js'
let route = 'api/conquest/admin/'

export default {
    async fetchSeveridades({commit}) {
        let res = await axios.get(route+'severidades')
        commit('SET_SEVERIDADES', res.data.items)
    },
    async fetchMedios({commit}) {
        let res = await axios.get(route+'medios')
        commit('SET_MEDIOS', res.data.items)
    },
    async fetchEstatus({commit}) {
        let res = await axios.get(route+'estatus')
        commit('SET_ESTATUS', res.data.items)
    },
    async fetchTiposReporte({commit}) {
        let res = await axios.get(route+'tipos_reporte')
        commit('SET_TIPOS_REPORTE', res.data.items)
    },
    async fetchUsuarios({commit}) {
        let res = await axios.get(route+'usuarios')
        commit('SET_USUARIOS', res.data.items)
    },
    async fetchOperadores({commit}) {
        let res = await axios.get(route+'usuarios/operadores')
        commit('SET_OPERADORES', res.data.items)
    },
    async fetchSoportes({commit}) {
        let res = await axios.get(route+'usuarios/soportes')
        commit('SET_SOPORTES', res.data.items)
    },
    async fetchDispositivos({commit}, {sitio}) {
        console.log(sitio)
        let res
        if(sitio){
            res = await axios.get(route+'dispositivos.findBySitio', {sitio:sitio})
        }else{
            res = await axios.get(route+'dispositivos')
        }

        commit('SET_DISPOSITIVOS', res.data.items)
    },

    async fetchSitios({commit}) {
        let res = await axios.get(route+'sitios')
        commit('SET_SITIOS', res.data.items)
    },

    async fetchPartidas({commit}) {
        let res = await axios.get(route+'dispositivos.partidas')
        commit('SET_PARTIDAS', res.data.partidas.items)
    },
}
