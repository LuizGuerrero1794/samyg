export default {
    SET_SEVERIDADES(state, payload) {
        state.severidades = payload
    },
    SET_MEDIOS(state, payload) {
        state.medios = payload
    },
    SET_ESTATUS(state, payload) {
        state.estatus = payload
    },
    SET_TIPOS_REPORTE(state, payload) {
        state.tipos_reporte = payload
    },
    SET_USUARIOS(state, payload) {
        state.usuarios = payload
    },
    SET_OPERADORES(state, payload) {
        state.operadores = payload
    },
    SET_SOPORTES(state, payload) {
        state.soportes = payload
    },
    SET_DISPOSITIVOS(state, payload) {
        state.dispositivos = payload
    },
    SET_SITIOS(state, payload) {
        state.sitios = payload
    },
    SET_PARTIDAS(state, payload) {
        state.partidas = payload
    },
}
