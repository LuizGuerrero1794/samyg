import axios from '@/plugins/axios.js'
let route = 'api/conquest/'

export default {
    async fetchTickets({commit}, ) {
        let res = await axios.get(route+'tickets')
        commit('SET_TICKETS', res.data.items)
    },
}
