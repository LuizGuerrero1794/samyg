export default {
    getTickets: state => {
        return state.tickets
    },

    getTicketsAtendidos: state => {
        return state.tickets.filter(
            ticket => ticket.estatus_id >= 2 | 0
        )
    },

    getTicketsAbiertos: state => {
        return state.tickets.filter(
            ticket => ticket.estatus_id < 2 | 0
        )
    },

    getTicketsFinalizados: state => {
        return state.tickets.filter(
            ticket => ticket.estatus_id === 3 | 0
        )
    },

    getTiempoPromedioEspera: () => {
        return 0;
        /*
        let tickets_atendidos = state.tickets.filter(
            ticket => ticket.estatus_id >= 2
        )

        let sum = 0;
        tickets_atendidos.forEach(function(item) {
            sum += new Date(item.tiempo_espera).getTime();
        });

        return sum;

         */
    },

    getTiempoPromedioAtencion: () => {
        return 0;
        /*
        let tickets_atendidos = state.tickets.filter(
            ticket => ticket.estatus_id >= 2
        )
        return tickets_atendidos
        return (tickets_atendidos.length === 0) ? 5 : (tickets_atendidos
            .reduce((total, current) => total + current.tiempo_atencion) / tickets_atendidos.length)

         */
    }
}
