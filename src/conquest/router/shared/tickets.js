//import this.axios from '@/config/this.axios.js'

export default {

    add(data) {
        return this.axios.post('tickets.store', data)
    },

    edit(id, data) {
        return this.axios.post('tickets.update', {ticket: id}, data)
    },

    delete(id) {
        return this.axios.delete('tickets.destroy', {ticket: id})
    },

    tiempoAtencion(data) {
        return this.axios.post('tickets.tiempo_atencion', data)
    },

    severidades(data) {
        return this.axios.post('tickets.severidades', data)
    },

    getDispositivosBySitio(sitio){
        return this.axios.get('dispositivos.findBySitio', {sitio:sitio})
    },

    cambiarEstatus(data){
        return this.axios.post('tickets.cambiar_estatus', data)
    }
}
