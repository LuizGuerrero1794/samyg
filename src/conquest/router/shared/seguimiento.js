//import axios from '@/config/axios.js'

export default {

    add(data) {
        return this.axios.post('seguimiento.store', data)
    },

    edit(id, data) {
        return this.axios.put('seguimiento.update', {seguimiento: id}, data)
    },

    delete(id) {
        return this.axios.delete('seguimiento.destroy', {seguimiento: id})
    },

}
