//import axios from '@/config/axios.js'

export default {


    add(data) {
        return this.axios.post('usuarios.store', data)
    },

    edit(id, data) {
        return this.axios.put('usuarios.update', {usuario: id}, data)
    },

    delete(id) {
        return this.axios.delete('usuarios.destroy', {usuario: id})
    },

}
