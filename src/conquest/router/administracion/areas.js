import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('v1/areas')
    },

    get(id) {
        return axios.get('v1/areas/' + id)
    },

    add(data) {
        return axios.post('v1/areas', data)
    },

    edit(id, data) {
        return axios.put('v1/areas/' + id, data)
    },

    delete(id) {
        return axios.delete('v1/areas/' + id)
    },

    getClues() {
        return axios.post('v1/areas/clues')
    },
    getAreas() {
        return axios.post('v1/areas/areas')
    },

    getResponsables(id){
        return axios.post('v1/areas/responsables', id)
    }

}
