import axios from '@/plugins/axios.js'
let route = 'api/conquest/admin/tiempos_solucion'
export default {
    add(data) {
        return axios.post(route, data)
    },
    edit(id, data) {
        return axios.put(route +'/'+ id , data)
    },
    delete(id) {
        return axios.delete(route +'/'+ id)
    },
}
