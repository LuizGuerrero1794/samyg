//import axios from '@/config/axios.js'

export default {

    add(data) {
        return this.axios.post('sitios.store', data)
    },

    edit(id, data) {
        return this.axios.put('sitios.update', {sitio: id}, data)
    },

    delete(id) {
        return this.axios.delete('sitios.destroy', {sitio: id})
    },

}
