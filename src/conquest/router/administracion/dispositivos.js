//import axios from '@/config/axios.js'

export default {

    add(data) {
        return this.axios.post('dispositivos.store', data)
    },

    edit(id, data) {
        return this.axios.put('dispositivos.update', {dispositivo: id}, data)
    },

    delete(id) {
        return this.axios.delete('dispositivos.destroy', {dispositivo: id})
    },

}
