//import Vue from 'vue';
//import VueRouter from "vue-router";

const Full = () => import('@/conquest/containers/admin/Full')
//const NotFound = () => import('@/conquest/components/NotFound')
const Forbidden = () => import('@/conquest/components/Forbidden')
const Home = () => import('@/conquest/views/Home')
const Dashboard = () => import('@/conquest/views/Dashboard')
const Usuarios = () => import('@/conquest/views/shared/Usuarios')
const Dispositivos = () => import('@/conquest/views/admin/Dispositivos')
const Severidades = () => import('@/conquest/views/admin/severidades/index')
const Medios = () => import('@/conquest/views/admin/medios/index')
const Sitios = () => import('@/conquest/views/admin/Sitios')
const TiposReporte = () => import('@/conquest/views/admin/tipos_reporte/index')
const Estatus = () => import('@/conquest/views/admin/estatus/index')
const TiemposAtencion = () => import('@/conquest/views/admin/tiempos_atencion/index')
const TiemposSolucion = () => import('@/conquest/views/admin/tiempos_solucion/index')

const Tickets = () => import('@/conquest/views/shared/Tickets')

//Vue.use(VueRouter);

export default [

        {
            path: '/noautorizado',
            name: 'noautorizado',
            component: Forbidden
        },
        {
            path: '/',
            name: 'Full',
            redirect: '/conquest/admin/inicio',
            component: Full,
            children: [
                {
                    path: 'conquest/admin/inicio',
                    name: 'inicio',
                    component: Dashboard,
                },
                {
                    path: 'tickets-admin',
                    name: 'tickets-admin',
                    component: Tickets,
                },
                {
                    path: 'conquest/admin/catalogos',
                    name: 'administracion',
                    component: Home,
                    children: [
                        {
                            path: 'users',
                            name: 'users',
                            component: Usuarios,
                        },
                        {
                            path: 'sitios',
                            name: 'sitios',
                            component: Sitios,
                        },
                        {
                            path: 'dispositivos',
                            name: 'dispositivos',
                            component: Dispositivos,
                        },
                        {
                            path: 'severidades',
                            name: 'severidades',
                            component: Severidades,
                        },
                        {
                            path: 'medios',
                            name: 'medios',
                            component: Medios,
                        },
                        {
                            path: 'estatus',
                            name: 'estatus',
                            component: Estatus,
                        },
                        {
                            path: 'tipos_reporte',
                            name: 'tipos_reporte',
                            component: TiposReporte,
                        },
                        {
                            path: 'tiempos_atencion',
                            name: 'tiempos_atencion',
                            component: TiemposAtencion,
                        },
                        {
                            path: 'tiempos_solucion',
                            name: 'tiempos_solucion',
                            component: TiemposSolucion,
                        },
                    ]
                },
            ]
        }
    ]

//export default router;
