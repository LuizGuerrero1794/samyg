import Vue from 'vue';
import VueRouter from "vue-router";

const Full = () => import('../containers/admin/Full')
const NotFound = () => import('../components/NotFound')
const Forbidden = () => import('../components/Forbidden')
const Home = () => import('../views/Home')
const Dashboard = () => import('../views/Dashboard')
const Usuarios = () => import('../views/shared/Usuarios')
const Dispositivos = () => import('../views/admin/Dispositivos')
const Severidades = () => import('../views/admin/Severidades')
const Medios = () => import('../views/admin/Medios')
const Sitios = () => import('../views/admin/Sitios')
const TiposReporte = () => import('../views/admin/TiposReporte')
const Estatus = () => import('../views/admin/Estatus')
const TiemposAtencion = () => import('../views/admin/TiemposAtencion')
const TiemposSolucion = () => import('../views/admin/TiemposSolucion')

const Tickets = () => import('../views/shared/Tickets')

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: '',
    base: document.querySelector('#app').getAttribute('data-path') || '/',
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/noautorizado',
            name: 'noautorizado',
            component: Forbidden
        },
        {
            path: '/admin/panel',
            name: 'Full',
            redirect: '/admin/panel/inicio',
            component: Full,
            children: [
                {
                    path: 'inicio',
                    name: 'inicio',
                    component: Dashboard,
                },
                {
                    path: 'tickets',
                    name: 'tickets',
                    component: Tickets,
                },
                {
                    path: 'administracion',
                    name: 'administracion',
                    component: Home,
                    children: [
                        {
                            path: 'users',
                            name: 'users',
                            component: Usuarios,
                        },
                        {
                            path: 'sitios',
                            name: 'sitios',
                            component: Sitios,
                        },
                        {
                            path: 'dispositivos',
                            name: 'dispositivos',
                            component: Dispositivos,
                        },
                        {
                            path: 'severidades',
                            name: 'severidades',
                            component: Severidades,
                        },
                        {
                            path: 'medios',
                            name: 'medios',
                            component: Medios,
                        },
                        {
                            path: 'estatus',
                            name: 'estatus',
                            component: Estatus,
                        },
                        {
                            path: 'tipos_reporte',
                            name: 'tipos_reporte',
                            component: TiposReporte,
                        },
                        {
                            path: 'tiempos_atencion',
                            name: 'tiempos_atencion',
                            component: TiemposAtencion,
                        },
                        {
                            path: 'tiempos_solucion',
                            name: 'tiempos_solucion',
                            component: TiemposSolucion,
                        },
                    ]
                },
            ]
        }
    ]
});

export default router;
