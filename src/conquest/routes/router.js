const Full = () => import('../containers/Full')
const NotFound = () => import('../components/NotFound')
const Forbidden = () => import('../components/Forbidden')
const Dashboard = () => import('../views/Dashboard2')
const Tickets = () => import('../views/shared/tickets/index')
const TicketDetalle = () => import('../views/shared/TicketDetalle')

    export default [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/noautorizado',
            name: 'noautorizado',
            component: Forbidden
        },
        {
            path: '/',
            name: 'Full',
            redirect: '/conquest/dashboard',
            component: Full,
            children: [
                {
                    path: '/conquest/dashboard',
                    name: 'dashboard',
                    component: Dashboard,
                },
                {
                    path: '/conquest/tickets',
                    name: 'tickets',
                    component: Tickets,
                },
                {
                    path: '/conquest/tickets/ticket_detalle/:ticket',
                    name: 'ticket_detalle',
                    component: TicketDetalle,
                },
            ]
        }
    ];
