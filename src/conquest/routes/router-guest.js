import Vue from 'vue';
import VueRouter from "vue-router";

const Full = () => import('../containers/guest/Full')
const NotFound = () => import('../components/NotFound')
const Forbidden = () => import('../components/Forbidden')
const Home = () => import('../views/Home')
const Dashboard = () => import('../views/Dashboard')

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: '',
    base: document.querySelector('#app').getAttribute('data-path') || '/',
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/noautorizado',
            name: 'noautorizado',
            component: Forbidden
        },
        {
            path: '/guest',
            name: 'Full',
            redirect: '/guest/dashboard',
            component: Full,
            children: [
                {
                    path: '/guest/dashboard',
                    name: 'inicio',
                    component: Dashboard,
                },
            ]
        }
    ]
});

export default router;
