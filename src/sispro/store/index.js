import Vue from 'vue'
import Vuex from 'vuex'

import sispro from './store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        sispro: sispro,
    }
})

export default vx;
