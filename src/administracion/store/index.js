import Vue from 'vue'
import Vuex from 'vuex'

import administracion from './store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        administracion: administracion,
    }
})

export default vx;
