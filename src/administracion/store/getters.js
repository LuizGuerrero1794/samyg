export default {
    sistema (state){
        return state.sistema;
    },
    usuario (state){
        return state.usuario;
    },
    mini (state){
        return state.mini;
    },
    sidebar (state){
        return state.sidebar;
    },
    configuracion (state){
        return state.configuracion;
    },
    catalogo_sitios (state){
        return state.catalogo_sitios;
    }
}
