export default {sistema: {
        bg_color: 'blue darken-4',
    },
    sidebar:[

    ],
    mini: false,
    title: 'SAMyG - ADMINISTRACIÓN',
    catalogo_usuarios: [
        { id:1, name: 'Luiz Guerrero', email: 'root@mail.com',},
    ],
    permisos_tabs: [
        { id: 1, name:'Nombre', label:"Etiqueta", parent: null },
    ],
    catalogo_proyectos: [],
    configuracion:[],
    catalogo_sitios: [],
}
