import Api from '@/components/Api.js';

export default {
    getSelectSitios({commit}){
        return Api.index('/api/admin/sitios').then(response=>{
            return commit('SET_SELECT_SITIOS',response.data);
        })
    },
}
