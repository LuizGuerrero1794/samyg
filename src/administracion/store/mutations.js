export default {
    SET_SELECT_SITIOS(state, data) {
        let sitios = [];
        data.items.map(s=>{
            sitios.push({id: s.id, text: s.nombre});
        });

        state.catalogo_sitios = sitios;
    },
}
