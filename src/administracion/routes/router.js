import store from '@/plugins/store.js'
import axios from '@/plugins/axios.js'

export default [
        {
            path: '/administracion',
            name: 'Administracion',
            redirect: '/proyectos/:id/administracion/inicio',
            beforeEnter(to, from, next){
                from;
                let token = localStorage.getItem('token')

                axios.defaults.headers = {'Authorization': `Bearer ${token}`}
                
                axios.get('/api/admin/proyectos/'+to.params.id).then((response)=>{
                    if(response.data.item){
                        store.commit('index/SET_PROYECTO',response.data.item)
                        next();
                    }else{
                        next('/')
                    }
                });
            },
            component: () => import('../Full'),
            children: [
                {
                    path: '/proyectos/:id/administracion/inicio',
                    name: 'Administracion-Inicio',
                    // component: Dashboard,
                },
                {
                    path: '/proyectos/:id/administracion/proyectos',
                    name: 'Administracion-Proyectos',
                    component: () => import('../views/Proyectos/Index.vue'),
                },
                {
                    path: '/proyectos/:id/administracion/administradores',
                    name: 'Administracion-Administradores',
                    component: () => import('../views/Administradores/Index.vue'),
                },
                {
                    path: '/proyectos/:id/administracion/usuarios',
                    name: 'Administracion-Usuarios',
                    component: () => import('../views/Usuarios/Index.vue'),
                },
                // {
                //     path: 'usuarios/permisos/:id',
                //     name: 'Administracion-Usuarios-Permisos',
                //     component: () => import('../views/Administradores/Permisos.vue'),
                // },
                // {
                //     path: 'usuarios/roles/:id',
                //     name: 'Administracion-Usuarios-Roles',
                //     component: () => import('../views/Usuarios/Roles.vue'),
                // },
                {
                    path: '/proyectos/:id/administracion/sitios',
                    name: 'Administracion-Sitios',
                    component: () => import('../views/Sitios/Index.vue'),
                },
                {
                    path: '/proyectos/:id/administracion/configuracion',
                    name: 'Administracion-Configuracion',
                    component: () => import('../views/Configuracion/Index.vue'),
                },
            ]
        }
    ]
