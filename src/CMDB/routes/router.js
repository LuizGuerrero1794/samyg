
const Dashboard = () => import('../views/Dashboard')

export default [
        {
            path: '/cmdb',
            name: 'CMDB',
            redirect: '/cmdb/inicio',
            component: () => import('../Full'),
            children: [
                {
                    path: 'inicio',
                    name: 'CMDB-Inicio',
                    component: Dashboard,
                },
            ]
        }
    ]
