import Vue from 'vue'
import Vuex from 'vuex'

import cmdb from './store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        cmdb: cmdb,
    }
})

export default vx;
