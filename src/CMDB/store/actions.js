import Api from "@/components/Api";
export default {
    getSelectSitios({commit}){
        return Api.index('/api/cmdb/sitios?_relations=direccion').then(response=>{
            return commit('SET_SELECT_SITIOS',response.data);
        })
    },
    getItems({commit}){
        return Api.index('/api/cmdb/items').then(response=>{
            return commit('SET_ITEMS',response.data);
        })
    }
}
