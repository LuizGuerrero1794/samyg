export default {
    SET_SELECT_SITIOS(state, data) {
        let sitios = [];

        data.items.map(s=>{
            s.name = s.nombre;
            if(s.ascendencia.length==0){
                sitios.push(s);
            }
            s.children = [];
        });

        state.catalogo_sitios = sitios;
    },
    SET_ITEMS(state,data) {
        state.catalogo_items = data.items;
        
    },
}
