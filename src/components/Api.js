import axios from '@/plugins/axios.js'

let token = localStorage.getItem('token')
axios.defaults.headers.common = {'Authorization': `Bearer ${token}`}

export default {
    dataTable(route,data) {
        return axios.post(route+'/datatables', data);
    },
    add(route, data) {
        return axios.post(route, data);
    },
    index(route) {
        return axios.get(route);
    },
    edit(route, id, data) {
        return axios.put(route +'/'+ id , data);
    },
    show(route, id) {
        return axios.get(route +'/'+ id);
    },
    delete(route,id) {
        return axios.delete(route +'/'+ id);
    },
    getDispositivosBySitio(route, sitio){
        return axios.get(route+'/dispositivos/findBySitio/' + sitio)
    },
    tiempoAtencion(route, data) {
        return axios.post(route+'/tiempo_atencion', data)
    },
    severidades(route, data) {
        return axios.post(route+'/severidades', data)
    },
    cambiarEstatus(route, data){
        return axios.post(route+'/cambiar_estatus', data)
    },
    addSeguimiento(route, data) {
        return axios.post(route, data)
    },
}
