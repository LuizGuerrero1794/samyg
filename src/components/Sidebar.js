export default {
    get(rol) {
        switch(rol){
            case 'super_administrador':
                return [
                    {icon: 'mdi-account-group', text: 'Administradores', route: 'Administracion-Administradores'},
                    {icon: 'mdi-cogs', text: 'Configuración', route: 'Administracion-Configuracion'},
                ];
            case 'adm_administrador':
                return [
                    {icon: 'mdi-account-group', text: 'Usuarios', route: 'Administracion-Usuarios'},
                    {icon: 'mdi-account-group', text: 'Sitios', route: 'Administracion-Sitios'},
                    {icon: 'mdi-cogs', text: 'Configuración', route: 'Administracion-Configuracion'},
                ];
            case 'cmdb_administrador':
                return [
                    {icon: 'mdi-inbox-multiple-outline', text: 'Infraestructura', route: 'Infraestructura'},
                    {icon: 'mdi-toolbox-outline', text: 'Servicios', route: 'tickets'},
                    { icon: 'mdi-devices', text: 'Dispositivos', route: 'Dispositivos'},
                    { icon: 'mdi-cogs', text: 'Configuración', route: 'Configuracion'},
                    // {
                    //     icon: 'mdi-cogs', text: 'Configuracion', parent: 'admin/panel/administracion',
                    //     children: [
                    //         {icon: 'mdi-account-group', text: 'Usuarios', route: 'users'},
                    //         {icon: 'mdi-office-building-marker', text: 'Sitios', route: 'sitios'},
                    //         {icon: 'mdi-devices', text: 'Dispositivos', route: 'dispositivos'},
                    //         {icon: 'mdi-alert-rhombus-outline', text: 'Severidades', route: 'severidades'},
                    //         {icon: 'mdi-highway', text: 'Medios', route: 'medios'},
                    //         {icon: 'mdi-state-machine', text: 'Estatus', route: 'estatus'},
                    //         {icon: 'mdi-format-list-bulleted-type', text: 'Tipos de reporte', route: 'tipos_reporte'},
                    //         {icon: 'mdi-timer', text: 'Tiempos de atención', route: 'tiempos_atencion'},
                    //         {icon: 'mdi-timetable', text: 'Tiempos de solución', route: 'tiempos_solucion'},
                    //     ]
                    // },
                ];
        }
    },
}
